from django.shortcuts import render
from django.http import JsonResponse
from json import JSONEncoder
from django.views.decorators.csrf import csrf_exempt
from .models import User, Token, Expense, Income
from datetime import datetime

# Create your views here.

@csrf_exempt
def submit_expense(request):
    """User submit an expense"""
    
    #TODO: validate data. User might be fake. Token might be fake. amount might be ...
    this_token = request.POST['Token']
    this_user = User.objects.filter(token__token = this_token).get()
    #TODO: User might wants submit another time
    if 'Date' in request.POST:
        date = request.POST['Date']
    else:
        date = datetime.now()
    Expense.objects.create(user = this_user, \
                           amount = request.POST['Amount'], \
                           text = request.POST['Text'],
                           date = date)
    
    return JsonResponse({
        'Status' : 'ok'
    }, encoder=JSONEncoder)

@csrf_exempt
def submit_income(request):
    """User submit an income"""
    
    #TODO: validate data. User might be fake. Token might be fake. amount might be ...
    this_token = request.POST['Token']
    this_user = User.objects.filter(token__token = this_token).get()
    #TODO: User might wants submit another time
    if 'Date' in request.POST:
        date = request.POST['Date']
    else:
        date = datetime.now()
    Income.objects.create(user = this_user, \
                           amount = request.POST['Amount'], \
                           text = request.POST['Text'],
                           date = date)
    
    return JsonResponse({
        'Status' : 'ok'
    }, encoder=JSONEncoder)
