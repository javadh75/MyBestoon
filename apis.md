/submit/expense/
    POST, returns a json
    input: Token, Text, Amount, Date(optional)
    output: status: ok

/submit/income/
    POST, returns a json
    input: Token, Text, Amount, Date(optional)
    output: status: ok